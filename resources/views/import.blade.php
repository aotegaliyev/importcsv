<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CSV Import</title>
</head>
<body>
    <div>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>BIN</th>
                    <th>Legal Name</th>
                    <th>Address</th>
                    <th>City</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $row)
                    <tr>
                        <td>{{$row->name}}</td>
                        <td>{{$row->bin}}</td>
                        <td>{{$row->legal_name}}</td>
                        <td>{{$row->address}}</td>
                        <td>{{$row->city}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <br><br>
    <form action="import" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="file" name="file" accept=".csv">
        <br><br>
        <button class="btn btn-success">Import File</button>
    </form>
</body>
</html>