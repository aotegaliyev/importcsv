<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CompaniesImport;
use DB;

class CompanyController extends Controller
{
    public function index()
    {
        $data = Company::get();
        return view('import', compact('data'));
    }
    public function import()
    {
        $data = Excel::toArray(new CompaniesImport, request()->file('file'));
        $data = $data[0];
        // dd($data);
        foreach($data as $item){
            DB::table('companies_copy_1')->whereNull('name')
                ->where('bin', $item['bin'])
                ->orWhere('legal_name', $item['legal_name'])
                ->orWhere('address', $item['address'])
                ->orWhere('city', $item['city'])
                ->update(['name' => $item['name']]);
        }
        foreach($data as $item){
            DB::table('companies_copy_1')->whereNull('bin')
                ->where('name', $item['name'])
                ->orWhere('legal_name', $item['legal_name'])
                ->orWhere('address', $item['address'])
                ->orWhere('city', $item['city'])
                ->update(['bin' => $item['bin']]);
        }
        foreach($data as $item){
            DB::table('companies_copy_1')->whereNull('legal_name')
                ->where('name', $item['name'])
                ->orWhere('bin', $item['bin'])
                ->orWhere('address', $item['address'])
                ->orWhere('city', $item['city'])
                ->update(['legal_name' => $item['legal_name']]);
        }
        foreach($data as $item){
            DB::table('companies_copy_1')->whereNull('address')
                ->where('name', $item['name'])
                ->orWhere('bin', $item['bin'])
                ->orWhere('legal_name', $item['legal_name'])
                ->orWhere('city', $item['city'])
                ->update(['address' => $item['address']]);
        }
        foreach($data as $item){
            DB::table('companies_copy_1')->whereNull('city')
            ->where('name', $item['name'])
            ->orWhere('bin', $item['bin'])
            ->orWhere('legal_name', $item['legal_name'])
            ->orWhere('address', $item['address'])
            ->update(['city' => $item['city']]);
        }
        
        return back();
    }
}
