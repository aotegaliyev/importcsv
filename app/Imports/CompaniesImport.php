<?php

namespace App\Imports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

class CompaniesImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Company([
            'name'          => $row['name'],
            'bin'           => $row['bin'],
            'legal_name'    => $row['legal_name'],
            'address'       => $row['address'],
            'city'          => $row['city'],
        ]);
    }
}
